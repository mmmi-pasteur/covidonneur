

data {
  
  int Nageclasses ;
  int N; 
  int Nsero ;
  int Nsero_total ;  // =  Nsero * Nageclasses
  int Nregions;
  
  real Infections[N, Nsero_total] ; 
  
  int X_Seroprevalences_number[Nsero_total]; 
  int N_Seroprevalences_number[Nsero_total]; 
  int Index_Seroprevalences_date[Nsero_total]; 
  int index_region[Nsero_total];
  int index_age[Nsero_total];
  //int index_infection[Nsero_total];
  real population_region[Nsero_total];
}

parameters {
  //  real<lower=0, upper=1> theta1[N];
  real<lower=0, upper=1> pH[Nageclasses]; 
  real<lower=0> days_decay ; 
  real<lower = 0> delta_negbin;
  real<lower=0> factor_region[Nregions] ; 
}

transformed parameters{
  real<lower=0> decay_rate ; 
  real<lower=0> seroprevalence_expected[Nsero_total];
  real S[N];
  real LP[Nsero_total]; 
  //real factor_region_pH[Nregions]; 
  
  decay_rate = log(2)/days_decay;
  
  for(j in 1:N){
    S[j] = exp(-decay_rate*(j-1));
  }
  
  for(i in 1:Nsero_total){
    seroprevalence_expected[i] =0;
    for(k in 1:Index_Seroprevalences_date[i]){
      seroprevalence_expected[i] =  seroprevalence_expected[i]  +Infections[k,i]*S[Index_Seroprevalences_date[i]-k+1]; 
    }
    seroprevalence_expected[i] =  factor_region[index_region[i]]/pH[index_age[i]]*seroprevalence_expected[i]/population_region[i];
  }
 
  for(i in 1:Nsero_total){
     //LP[i] = neg_binomial_2_lpmf( X_Seroprevalences_number[i] |  seroprevalence_expected[i]*N_Seroprevalences_number[i],  pow(seroprevalence_expected[i]*N_Seroprevalences_number[i],0.2) );
     LP[i] = poisson_lpmf( X_Seroprevalences_number[i] |  seroprevalence_expected[i]*N_Seroprevalences_number[i]) ;
     
  }
}

model {
  
  for(i in 1:Nageclasses){
    pH[i] ~ uniform(0,1);
  }    
  days_decay ~ uniform(1,2000);
  delta_negbin ~ uniform(0,2); 
  
  for(i in 1:Nregions){
    factor_region[i] ~ uniform(0,30);
  }
  
  for(i in 1:Nsero_total){
    target += LP[i] ;
    // binomial_lpmf(X_Seroprevalences_number[i] | N_Seroprevalences_number[i],  seroprevalence_expected[i] );
  }
  
}
