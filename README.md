# Covidonneur

Codes and Data for the paper “SARS-CoV-2 IgG seroprevalence surveys in blood donors before the vaccination campaign, France 2020-2021”.

## Overview

This repository contains scripts to
- [ ] Read and format the serological data.
- [ ] Launch MCMC scripts to fit models of infection and antibody decay and estimate parameters of infection-hospitalisation ratio (IHR) and force of infection in each region. 
- [ ] Infer prevalence and plot the output of the MCMC
The first script to be launched is Setup_data.R; The model is launch with the script launch_analysis.


## Data

The folder Data contains the seroneutralisation result for 32605 samples acquired from blood donors in 2020-2021 in metropolitan France (Covdon_v3.csv). Data were aggregated in a format that maintains anonymity of the participants: For each individual, we provide the age group (in groups 18-30, 31-40, 41-50, 51-60, 61-70), department, survey period and median date of the survey. Additional necessary data are provided: the population size by age and region, the public daily hospital admissions.  


## Scripts

All analyses can be run from the main.R file, which calls scripts to run the analysis (contained in the scripts folder) and functions (contained in the R folder). The purpose of each R file is described below.

- [ ] main.R Launch all the necessary codes
- [ ] Setup_data.R  Setup the data for further analysis
- [ ] functions.R Some usefuls functions
- [ ] launch_MCMC.R Launch the MCMC scripts. In the directory StanModels three models are provided. 
- [ ] posthoc_analysis.R A script containing some  posthoc  analysis of the MCMC; It allows plotting the IHR by age and the decay of the assay capacity as a function of the time since seroconversion. 
- [ ] IHR_Covidonneur.R Plot the IHR as estimated by the MCMC script. Here, a chain was uploaded (Model3b.rds and Model3f.rds).
- [ ] Seroprevalence_age.R This scripts plots the observed seroprevalence, the reconstructed number of infection, the inferred seroprevalence given by the exponential decay of the antibody response following infection.

 
