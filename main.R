library(ggplot2)
library(dplyr)
library(lubridate)
library(stringr)
library(binom)
library(ggpubr)
library(gridExtra)
library(rstan)
library(RColorBrewer)

Sys.setlocale("LC_ALL", "English")
# Load some functions
source('functions.R')

# set up data
source('Setup_data.R')

# Inference of the parameters of the model using an MCMC method
source('launch_MCMC.R')

# posthoc analysis
source('posthoc_analysis.R')
