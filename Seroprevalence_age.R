# Seroprevalence in the different age groups
# Compare the observed seroprevalence, the reconstructed number of infection, 
# the inferred seroprevalence given by the exponential decay of the antibody response following infection.


sero_expected <- function(reg,a, decay_model =1){
  
  index.region=which(data$regions==reg)
  w2=which( data$index_region == index.region)
  
  ages.index = rep(0, data$Nageclasses)
  for(i in 1:data$Nageclasses){
    ages.index[i] =  w2[which(data$index_age[w2] == i)[1]]
  } 
  Infections = data$Infections[, ages.index]
  POP =  data$population_region[ages.index]
  
  M =nrow(Chains$pH)
  Infected=array(data = 0, dim=c(M, data$N))
  Infected.counted=array(data = 0, dim=c(M, data$N))
  
  Sero =   data$Seroprevalences %>% filter(region == reg & age == a) 
  
  seroprevalence_expected=array(data = 0, dim=c(M, data$N))
  seroprevalence_corrected=array(data = 0, dim=c(M, data$N))
  
  D0 = Infections[,a]
  population = POP[a]
  #  cumulative Infections
  for(k in 1:M ){
    ph= Chains$pH[k,a]
    f = Chains$factor_region[k, index.region]
    Infected[k, ] =   f*D0/ph/population
  }
  
  # seroprevalence expected
  # decay_model =1  avec perte d'anticorps
  # decay_model = 2 sans perte d'anticorps
  for(k in 1:M){
    if(decay_model==1){
      days_decay = Chains$days_decay[k]
      decay_rate =  log(2)/days_decay
      ex = exp(-decay_rate)
    }else{
      ex=1    
    }
    
    INF=Infected[k,]
    S=0*INF
    S[1]=INF[1]
    for( I in 2:data$N){
      S[I] =  INF[I] + S[I-1]*ex
    }
    seroprevalence_expected[k,] =seroprevalence_expected[k,] + S
    #   seroprevalence_corrected[k,] = S*Chains$sensitivity[k]+(1-S)*(1-Chains$specificity[k])
  }
  return(apply(seroprevalence_expected,2,mean.and.ci))
  
}




S = data$Seroprevalences
df.Sero =NULL
for(a in 1:data$Nageclasses){
  for( p in 1:6){
    s = S %>% filter(period == p & age ==a )
    #for(k in 1:nrow(s)){
    n =sum(s$n)
    x=sum(s$x)
    b=binom.confint(x = x,n=n,method='exact')
    df.Sero = rbind(df.Sero, data.frame(period = p, age=a, mean = 100*b$mean , lwr = 100*b$lower, upr= 100*b$upper))
    #}
  }
}

df.Sero$period = as.factor(df.Sero$period)

Period=6

# real observed seroprevalence hardcoded here
df.Sero = NULL
n =c(3762,2191,2475,2177,1411)
x=c(410,146,181,172,77)
for(a in 1:5){
  
  b=binom.confint(x[a],n[a], method='exact')
  df.Sero=rbind(df.Sero, data.frame(period=6,age = a, mean =100*b$mean,   lwr = 100*(b$lower), upr=100*(b$upper) ))
}

g=ggplot(df.Sero)  + geom_point(aes(x= age, y=mean), color='red')+ 
  geom_segment(aes(x=age, xend=age, y = lwr, yend =upr), color='red')+
  geom_line(aes(x= age, y=mean, color='red'))+
  theme_bw() +
  theme(axis.text.x = element_text(size=16),
        axis.text.y = element_text(size=16),
        text=element_text(size=16))+
  scale_x_continuous(breaks = seq(1,data$Nageclasses),
                     labels = Covidonneur_ages) +
  ylim(c(0,NA))

print(g) 


##  plot seroprevalence ------

# period <-> date

date  = round(median(data$Seroprevalence$n.date.prelevement[which(data$Seroprevalences$period==Period)]))

k=0
date.period=c()
for(period in 2:6){
  k=k+1
  date.period[k]=round(median(data$Seroprevalence$n.date.prelevement[which(data$Seroprevalences$period==period)]))
}
 

regions =  data$regions
Inf1 = data$Infections.region.age %>% filter(region %in% regions)

population.reg = population.region %>% filter(region %in% regions)
df.Sero.model =NULL
df.Sero.exposed.model  = NULL
for(age in 1:data$Nageclasses){
  
  k=0
  Sero=0
  Sero.exposed = 0
  for(reg in regions){
    k=k+1
    # for(age in 1:Nageclasses){
    S = sero_expected(reg, a = age, decay_model=1)
    Sero = Sero + population.reg[k, age+1]/sum(population.reg[,age+1]) *S
    S = sero_expected(reg, a = age, decay_model=2)
    Sero.exposed= Sero.exposed + population.reg[k, age+1]/sum(population.reg[,age+1]) *S#}
  }
  
  for(Period in 2:6){
    date = date.period[Period-1]
    df.Sero.model=rbind(df.Sero.model, data.frame(period=Period, 
                                                  age= age,
                                                  mean = Sero[1,date],
                                                  lwr = Sero[2,date],
                                                  upr = Sero[3,date]))
    
    
    df.Sero.exposed.model=rbind(df.Sero.exposed.model, data.frame(period=Period, 
                                                                  age= age,
                                                                  mean = Sero.exposed[1,date],
                                                                  lwr = Sero.exposed[2,date],
                                                                  upr = Sero.exposed[3,date]))
    
  }
}

offset1 = -0.1
offset2=-0.2

Period =6

df.Sero2 =df.Sero %>% filter(period==Period)
D2 = df.Sero.model%>% filter(period == Period)
D3 = df.Sero.exposed.model%>% filter(period == Period)

g=ggplot(df.Sero2)  + geom_point(aes(x= age, y=mean), color='black')+ 
  geom_segment(aes(x=age, xend=age, y = lwr, yend =upr), color='black')+
  geom_line(aes(x= age, y=mean), color='black')+
  theme_bw() +
  theme(axis.text.x = element_text(size=16),
        axis.text.y = element_text(size=16),
        text=element_text(size=16))+
  scale_x_continuous(breaks = seq(1,data$Nageclasses),
                     labels = Covidonneur_ages) +
  ylim(c(0,NA))+
  geom_point(data= D2,aes(x= age+offset1, y=100*mean), color='red')+ 
  geom_segment(data= D2, aes(x=age+offset1, xend=age+offset1, y = 100*lwr, yend =100*upr), color='red')+
  geom_line(data= D2,aes(x= age+offset1, y=mean*100, color=period), color='red')+
  geom_point(data= D3,aes(x= age+offset2, y=100*mean), color='aquamarine4')+ 
  geom_segment(data= D3, aes(x=age+offset2, xend=age+offset2, y = 100*lwr, yend =100*upr), color='aquamarine4')+
  geom_line(data= D3,aes(x= age+offset2, y=mean*100, color=period), color='aquamarine4')+ylab('Proportion (%)')

print(g) 



