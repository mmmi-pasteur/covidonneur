
## Posthoc analysis for the retained model ----

F1=readRDS('Results/Model3b.rds')
Chains=F1$Chains
data=F1$data

### Infection Hospitalisation ratio ----
source('IHR_Covidonneur.R')

## Decay par age ----

decay_plot <- function(D,Envelope){
  g=ggplot(D )+  ggplot2::geom_polygon(data=Envelope, ggplot2::aes(date, y), fill='black', alpha =0.3)+
    geom_line(aes(x=date, y=y1), color='black') 
  
  g=g+ scale_x_continuous(breaks = 30*seq(0,8,by=2),
                          labels = c('0 mo','2 mo','4 mo','6 mo','8 mo'))
  g=g+ylim(c(0,100))+  theme_bw() +
    theme(axis.text.x = element_text(size=16),
          axis.text.y = element_text(size=16),
          text=element_text(size=16))+
    xlab(' ')+
    ylab('Assay sensitivity (%)') 
   return(g)
  
} 

M =nrow(Chains$decay_rate)
Days = seq(0, 30*9)
sensitivity1=array(data = 0, dim=c(M, length(Days)))

for(k in 1:M ){
  sensitivity1[k, ] = exp(-Chains$decay_rate[k]*Days)*100
}

sensitivity1=apply(sensitivity1, 2, mean.and.ci)

mean.and.ci(Chains$days_decay/30)

D1=data.frame(date = Days,
              y1= sensitivity1[1,], 
              y2 =sensitivity1[2,],
              y3=sensitivity1[3,])
D1.envelope = data.frame(date = c( Days , rev(Days)),
                         y = c( D1$y2, rev(D1$y3)))

G = decay_plot(D1,D1.envelope)
print(G)

# compare seroprevalence by age at Period = 6 ----

source('Seroprevalence_age.R')


